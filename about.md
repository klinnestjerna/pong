---
layout: page
title: About
permalink: /about/
---

SSG Table tennis List

To edit this list please clone the repo from git clone `https://gitlab.com/klinnestjerna/pong.git` and edit 

`_data/persons.yml`

example

```yml

- id: 1
  name: Kristofer Linnestjerna
  win: 0
  loss: 1

- id: 2
  name: Mattias Lasu
  win: 1
  loss: 0

```
